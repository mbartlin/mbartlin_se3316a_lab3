<!DOCTYPE html>
<html lang="en">
  <head>
   
   <meta http-equiv="Content-Type" content="text/html;charset=us-ansi">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <title>Lab3 SE3316A</title>
     
     <link href='https://fonts.googleapis.com/css?family=Cuprum|Cookie' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Shadows+Into+Light' rel='stylesheet' type='text/css'>

    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet">


    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Custom styles for this template -->
    <link href="carousel.css" rel="stylesheet">
  </head>
  <body>
    <div class="navbar-wrapper">
      <div class="container">

        <div class="navbar navbar-static-top" role="navigation">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="#">LAB 3</a>
            </div>
            <div class="navbar-collapse collapse">
              <ul class="nav navbar-nav">
                <li class="active"><a href="index.php">Home</a></li>
                <li><a href="about.php">About</a></li>
                <li><a href="work.php">Work</a></li>  
                <li><a href="artists.php">Artists</a></li>                
              </ul>
            </div>
          </div>
        </div>

      </div>
    </div>
    



    <!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
        <li data-target="#myCarousel" data-slide-to="3"></li>
        <li data-target="#myCarousel" data-slide-to="4"></li>
      </ol>
     <div class="carousel-inner">
      
      <?php
           $paintings = file('data-files/paintings.txt') or die('Error: cannot find file');
           $delimiter = '~';
           $counter = 0;
           $itemclass;
           foreach ($paintings as $painting)
           {
               $paintingfields = explode($delimiter, $painting);
            if ($paintingfields[3] == '01330')
            {
                
           $painting1ID =  $paintingfields[3] ; 
           $painting1Title =  $paintingfields[4] ;
            $painting1Description =  $paintingfields[5] ;
            $painting1Year =  $paintingfields[6] ;
            $painting1Width =  $paintingfields[7] ;
            $painting1Height =  $paintingfields[8] ;
           $painting1Genre =  $paintingfields[9] ;
            $painting1Gallery =  $paintingfields[10] ;
            $painting1Price =  $paintingfields[11] ;
            $painting1URL = $paintingfields[12];
            }
            
            if ($paintingfields[3] == '01080')
            {
                
           $painting2ID =  $paintingfields[3] ; 
           $painting2Title =  $paintingfields[4] ;
            $painting2Description =  $paintingfields[5] ;
            $painting2Year =  $paintingfields[6] ;
            $painting2Width =  $paintingfields[7] ;
            $painting2Height =  $paintingfields[8] ;
           $painting2Genre =  $paintingfields[9] ;
            $painting2Gallery =  $paintingfields[10] ;
            $painting2Price =  $paintingfields[11] ;
            $painting2URL = $paintingfields[12];
            }
            if ($paintingfields[3] == '01150')
            {
                
           $painting3ID =  $paintingfields[3] ; 
           $painting3Title =  $paintingfields[4] ;
            $painting3Description =  $paintingfields[5] ;
            $painting3Year =  $paintingfields[6] ;
            $painting3Width =  $paintingfields[7] ;
            $painting3Height =  $paintingfields[8] ;
           $painting3Genre =  $paintingfields[9] ;
            $painting3Gallery =  $paintingfields[10] ;
            $painting3Price =  $paintingfields[11] ;
            $painting3URL = $paintingfields[12];
            }
            if ($paintingfields[3] == '01180')
            {
                
           $painting4ID =  $paintingfields[3] ; 
           $painting4Title =  $paintingfields[4] ;
            $painting4Description =  $paintingfields[5] ;
            $painting4Year =  $paintingfields[6] ;
            $painting4Width =  $paintingfields[7] ;
            $painting4Height =  $paintingfields[8] ;
           $painting4Genre =  $paintingfields[9] ;
            $painting4Gallery =  $paintingfields[10] ;
            $painting4Price =  $paintingfields[11] ;
            $painting4URL = $paintingfields[12];
            }
            
             if ($paintingfields[3] == '01190')
            {
                
           $painting5ID =  $paintingfields[3] ; 
           $painting5Title =  $paintingfields[4] ;
            $painting5Description =  $paintingfields[5] ;
            $painting5Year =  $paintingfields[6] ;
            $painting5Width =  $paintingfields[7] ;
            $painting5Height =  $paintingfields[8] ;
           $painting5Genre =  $paintingfields[9] ;
            $painting5Gallery =  $paintingfields[10] ;
            $painting5Price =  $paintingfields[11] ;
            $painting5URL = $paintingfields[12];
            }
            
            if ($paintingfields[3] == '01220')
            {
                
           $painting6ID =  $paintingfields[3] ; 
           $painting6Title =  $paintingfields[4] ;
            $painting6Description =  $paintingfields[5] ;
            $painting6Year =  $paintingfields[6] ;
            $painting6Width =  $paintingfields[7] ;
            $painting6Height =  $paintingfields[8] ;
           $painting6Genre =  $paintingfields[9] ;
            $painting6Gallery =  $paintingfields[10] ;
            $painting6Price =  $paintingfields[11] ;
            $painting6URL = $paintingfields[12];
            
            $description = explode('.',$painting6Description);
            $painting6Description = $description[0].'.';
            
            }
            if ($paintingfields[3] == '01140')
            {
                
           $painting7ID =  $paintingfields[3] ; 
           $painting7Title =  $paintingfields[4] ;
            $painting7Description =  $paintingfields[5] ;
            $painting7Year =  $paintingfields[6] ;
            $painting7Width =  $paintingfields[7] ;
            $painting7Height =  $paintingfields[8] ;
           $painting7Genre =  $paintingfields[9] ;
            $painting7Gallery =  $paintingfields[10] ;
            $painting7Price =  $paintingfields[11] ;
            $painting7URL = $paintingfields[12];
            
            $description = explode('.',$painting7Description);
            $painting7Description = $description[0].'.';
            
            }
            if ($paintingfields[3] == '01260')
            {
                
           $painting8ID =  $paintingfields[3] ; 
           $painting8Title =  $paintingfields[4] ;
            $painting8Description =  $paintingfields[5] ;
            $painting8Year =  $paintingfields[6] ;
            $painting8Width =  $paintingfields[7] ;
            $painting8Height =  $paintingfields[8] ;
           $painting8Genre =  $paintingfields[9] ;
            $painting8Gallery =  $paintingfields[10] ;
            $painting8Price =  $paintingfields[11] ;
            $painting8URL = $paintingfields[12];
            
            $description = explode('.',$painting8Description);
            $painting8Description = $description[0].'.';
            }
            
            if ($paintingfields[3] == '01280')
            {
                
           $painting9ID =  $paintingfields[3] ; 
           $painting9Title =  $paintingfields[4] ;
            $painting9Description =  $paintingfields[5] ;
            $painting9Year =  $paintingfields[6] ;
            $painting9Width =  $paintingfields[7] ;
            $painting9Height =  $paintingfields[8] ;
           $painting9Genre =  $paintingfields[9] ;
            $painting9Gallery =  $paintingfields[10] ;
            $painting9Price =  $paintingfields[11] ;
            $painting9URL = $paintingfields[12];
            
            $description = explode('.',$painting9Description);
            $painting9Description = $description[0].'.';
            }
            if ($paintingfields[3] == '01290')
            {
                
           $painting10ID =  $paintingfields[3] ; 
           $painting10Title =  $paintingfields[4] ;
            $painting10Description =  $paintingfields[5] ;
            $painting10Year =  $paintingfields[6] ;
            $painting10Width =  $paintingfields[7] ;
            $painting10Height =  $paintingfields[8] ;
           $painting10Genre =  $paintingfields[9] ;
            $painting10Gallery =  $paintingfields[10] ;
            $painting10Price =  $paintingfields[11] ;
            $painting10URL = $paintingfields[12];
            
            $description = explode('.',$painting10Description);
            $painting10Description = $description[0].'.';
            
            }
            if ($paintingfields[3] == '01070')
            {
                
           $painting11ID =  $paintingfields[3] ; 
           $painting11Title =  $paintingfields[4] ;
            $painting11Description =  $paintingfields[5] ;
            $painting11Year =  $paintingfields[6] ;
            $painting11Width =  $paintingfields[7] ;
            $painting11Height =  $paintingfields[8] ;
           $painting11Genre =  $paintingfields[9] ;
            $painting11Gallery =  $paintingfields[10] ;
            $painting11Price =  $paintingfields[11] ;
            $painting11URL = $paintingfields[12];
            
            $description = explode('.',$painting11Description);
            $painting11Description = $description[0].'.';
            }
            
        
        $counter++;
           }
           
           
           echo("
            <div class=\"carousel-inner\">
       
      <div class='item active'><img src=\"art-images/paintings/medium/$painting1ID.jpg\" alt=\"$painting1Title\" title=\"$painting1Title\" rel=\"#PaintingThumb\" /><div class='container'><div class='carousel-caption'><h1>$painting1Title</h1><p>$painting1Year</p><p><a class='btn btn-lg btn-primary' href='$painting1URL
' role='button'>Learn more</a></p></div></div></div><div class='item'><img src=\"art-images/paintings/medium/$painting2ID.jpg\" alt=\"$painting2Title\" title=\"$painting2Title\" rel=\"#PaintingThumb\" /><div class='container'><div class='carousel-caption'><h1>$painting2Title</h1><p>$painting2Year</p><p><a class='btn btn-lg btn-primary' href='$painting2URL
' role='button'>Learn more</a></p></div></div></div><div class='item'><img src=\"art-images/paintings/medium/$painting3ID.jpg\" alt=\"$painting3Title\" title=\"$painting3Title\" rel=\"#PaintingThumb\" /><div class='container'><div class='carousel-caption'><h1>$painting3Title</h1><p>$painting3Year</p><p><a class='btn btn-lg btn-primary' href='$painting3URL
' role='button'>Learn more</a></p></div></div></div><div class='item'><img src=\"art-images/paintings/medium/$painting4ID.jpg\" alt=\"$painting4Title\" title=\"$painting4Title\" rel=\"#PaintingThumb\" /><div class='container'><div class='carousel-caption'><h1>$painting4Title</h1><p>$painting4Year</p><p><a class='btn btn-lg btn-primary' href='$painting4URL
' role='button'>Learn more</a></p></div></div></div><div class='item'><img src=\"art-images/paintings/medium/$painting5ID.jpg\" alt=\"$painting5Title\" title=\"$painting5Title\" rel=\"#PaintingThumb\" /><div class='container'><div class='carousel-caption'><h1>$painting5Title</h1><p>$painting5Year</p><p><a class='btn btn-lg btn-primary' href='$painting5URL
' role='button'>Learn more</a></p></div></div></div>    </div>
      <a class=\"left carousel-control\" href=\"#myCarousel\" role=\"button\" data-slide=\"prev\"><span class=\"glyphicon glyphicon-chevron-left\"></span></a>
      <a class=\"right carousel-control\" href=\"#myCarousel\" role=\"button\" data-slide=\"next\"><span class=\"glyphicon glyphicon-chevron-right\"></span></a>
    </div>
    
<br>
<br>
<br>

    <div class=\"container marketing\">

      <!-- Three columns of text below the carousel -->
      <div class=\"row\">
        <div class=\"col-lg-4\"><img class=\"img-circle\" src=\"art-images/paintings/medium/$painting6ID.jpg\" alt=\"$painting6Title\" title=\"$painting6Title\" style=\"width:100px; height:100px;\" /><h2>$painting6Title</h2><p class=\"text-justify\"> $painting6Description</p><p><a class=\"btn btn-default\" href='$painting6URL
' role=\"button\">View details &raquo;</a></p></div><!-- /.col-lg-4 --><div class=\"col-lg-4\"><img class=\"img-circle\" src=\"art-images/paintings/medium/$painting7ID.jpg\" alt=\"$painting7Title\" title=\"$painting7Title\" style=\"width:100px; height:100px;\" /><h2>$painting7Title</h2><p class=\"text-justify\"> $painting7Description</p><p><a class=\"btn btn-default\" href='$painting7URL
' role=\"button\">View details &raquo;</a></p></div><!-- /.col-lg-4 --><div class=\"col-lg-4\"><img class=\"img-circle\" src=\"art-images/paintings/medium/$painting8ID.jpg\" alt=\"$painting8Title\" title=\"$painting8Title\" style=\"width:100px; height:100px;\" /><h2>$painting8Title</h2><p class=\"text-justify\"> $painting8Description</p><p><a class=\"btn btn-default\" href='$painting8URL
' role=\"button\">View details &raquo;</a></p></div><!-- /.col-lg-4 -->      </div><!-- /.row -->

<div class=\"row\">
        <div class=\"col-lg-4\"><img class=\"img-circle\" src=\"art-images/paintings/medium/$painting9ID.jpg\" alt=\"$painting9Title\" title=\"$painting9Title\" style=\"width:100px; height:100px;\" /><h2>$painting9Title</h2><p class=\"text-justify\">$painting9Description</p><p><a class=\"btn btn-default\" href='$painting9URL
' role=\"button\">View details &raquo;</a></p></div><!-- /.col-lg-4 --><div class=\"col-lg-4\"><img class=\"img-circle\" src=\"art-images/paintings/medium/$painting10ID.jpg\" alt=\"$painting10Title\" title=\"$painting10Title\" style=\"width:100px; height:100px;\" /><h2>$painting10Title</h2><p class=\"text-justify\">$painting10Description</p><p><a class=\"btn btn-default\" href='$painting10URL
' role=\"button\">View details &raquo;</a></p></div><!-- /.col-lg-4 --><div class=\"col-lg-4\"><img class=\"img-circle\" src=\"art-images/paintings/medium/$painting11ID.jpg\" alt=\"$painting11Title\" title=\"$painting11Title\" style=\"width:100px; height:100px;\" /><h2>$painting11Title</h2><p class=\"text-justify\"> $painting11Description</p><p><a class=\"btn btn-default\" href='$painting11URL
' role=\"button\">View details &raquo;</a></p></div><!-- /.col-lg-4 -->      </div><!-- /.row -->

    </div><!-- /.container -->
           
           ");

?>




    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    
    
    
    
      </body>
</html>